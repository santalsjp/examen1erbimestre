//
//  Pelicula.swift
//  ExamenIOS
//
//  Created by Santiago Pazmiño on 5/12/17.
//  Copyright © 2017 Santiago Pazmiño. All rights reserved.
//


import Foundation
import ObjectMapper
class Pelicula:Mappable{
    
    var pkName:String?
    var pkGenero:String?
    var pkWeb:String?
    var pkSinopsis:String?
    var pkImage:String?
    var pkProduction:String?

    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        pkName <- map["title"]
        pkGenero <- map["genres.0.name"]
        pkWeb <- map["homepage"]
        pkSinopsis <- map["overview"]
        pkImage <- map["poster_path"]
        pkProduction <- map["production_companies.0.name"]
        
    }
    
}
