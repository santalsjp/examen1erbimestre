//
//  InfoThorViewController.swift
//  ExamenIOS
//
//  Created by Santiago Pazmiño on 5/12/17.
//  Copyright © 2017 Santiago Pazmiño. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class InfoThorViewController: UIViewController {
    
    
    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var nombrePeli: UILabel!
    
    @IBOutlet weak var reviewPeli: UITextView!
    @IBOutlet weak var homepagePeli: UILabel!
    @IBOutlet weak var generoPeli: UILabel!
    
    
    @IBOutlet weak var companyPelicula: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getVideo(videoCode: "z2dbvyV5jZU")
        Alamofire.request("https://api.themoviedb.org/3/movie/284053?api_key=70eb000eb294bf20031c5fe4afaca3fe&language=en-US").responseObject { (response: DataResponse<Pelicula>) in
            let pelicula = response.result.value
            
            DispatchQueue.main.async {
                self.nombrePeli.text = pelicula?.pkName
                self.generoPeli.text = pelicula?.pkGenero
                self.homepagePeli.text = pelicula?.pkWeb
                self.reviewPeli.text = pelicula?.pkSinopsis
                self.companyPelicula.text = pelicula?.pkProduction
                // self.imagePelicula.animationImages = "\(pelicula?.pkImage?? 0)"
                
            }
            
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getVideo(videoCode:String){
        let url = URL(string: "https://www.youtube.com/embed/\(videoCode)")
        webView.loadRequest(URLRequest(url:url!))
    }
    
    

}
