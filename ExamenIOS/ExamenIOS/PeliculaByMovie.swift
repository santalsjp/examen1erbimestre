//
//  PeliculaByMovie.swift
//  ExamenIOS
//
//  Created by Santiago Pazmiño on 12/12/17.
//  Copyright © 2017 Santiago Pazmiño. All rights reserved.
//

import Foundation
import ObjectMapper
class PeliculaByMovie:Mappable{
    
    var pkName:String?
    var pkGenero:String?
    var pkWeb:String?
    var pkSinopsis:String?
    var pkImage:String?
    var pkProduction:String?
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        pkName <- map["results.0.title"]
        pkGenero <- map["results.0.release_date"]
        pkWeb <- map["homepage"]
        pkSinopsis <- map["results.0.overview"]
        pkImage <- map["results.0.poster_path"]
        pkProduction <- map["production_companies.0.name"]
        
    }
    
}

