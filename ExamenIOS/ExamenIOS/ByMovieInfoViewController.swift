//
//  ByMovieInfoViewController.swift
//  ExamenIOS
//
//  Created by Santiago Pazmiño on 12/12/17.
//  Copyright © 2017 Santiago Pazmiño. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import AlamofireImage

class ByMovieInfoViewController: UIViewController {
    
    
    @IBOutlet weak var tituloPelicula: UILabel!
   // @IBOutlet weak var webPelicula: UILabel!
    @IBOutlet weak var byMoviePelicula: UITextField!
    @IBOutlet weak var reviewPelicula: UITextView!
    //@IBOutlet weak var companyPelicula: UILabel!
    @IBOutlet weak var generoPelicula: UILabel!
    
    @IBOutlet weak var imagePelicula: UIImageView!
    @IBAction func searchPelicula(_ sender: Any) {
       // var image2:String
        Alamofire.request("https://api.themoviedb.org/3/search/movie?api_key=70eb000eb294bf20031c5fe4afaca3fe&language=en-US&query=" + self.byMoviePelicula.text! + "&page=1&include_adult=false").responseObject { (response: DataResponse<PeliculaByMovie>) in
            let pelicula = response.result.value
            
            DispatchQueue.main.async {
                self.tituloPelicula.text = pelicula?.pkName
                self.generoPelicula.text = pelicula?.pkGenero
             //   self.webPelicula.text = pelicula?.pkWeb
                self.reviewPelicula.text = pelicula?.pkSinopsis
                var image1 = pelicula?.pkImage ?? ""
                
                Alamofire.request("https://image.tmdb.org/t/p/w500/" + image1).responseImage
                    {
                        response
                        in
                        guard let image = response.result.value
                            else
                        {
                            return
                        }
                        self.imagePelicula.image = image
                        
                        
                }
             //   self.companyPelicula.text = pelicula?.pkProduction
                // self.imagePelicula.animationImages = "\(pelicula?.pkImage?? 0)"
                
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
