//
//  InfoViewController.swift
//  ExamenIOS
//
//  Created by Santiago Pazmiño on 5/12/17.
//  Copyright © 2017 Santiago Pazmiño. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class InfoViewController: UIViewController {

    @IBOutlet weak var nombrePelicula: UILabel!
    @IBOutlet weak var generoPelicula: UILabel!
    
    @IBOutlet weak var webPelicula: UILabel!
    

    @IBOutlet weak var sinopsisPelicula: UITextView!
    
    @IBOutlet weak var companyPelicula: UILabel!
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getVideo(videoCode: "DblEwHkde8U")
        Alamofire.request("https://api.themoviedb.org/3/movie/141052?api_key=70eb000eb294bf20031c5fe4afaca3fe&language=en-US").responseObject { (response: DataResponse<Pelicula>) in
            let pelicula = response.result.value
      
            DispatchQueue.main.async {
                self.nombrePelicula.text = pelicula?.pkName
                self.generoPelicula.text = pelicula?.pkGenero
                self.webPelicula.text = pelicula?.pkWeb
                self.sinopsisPelicula.text = pelicula?.pkSinopsis
                self.companyPelicula.text = pelicula?.pkProduction
               // self.imagePelicula.animationImages = "\(pelicula?.pkImage?? 0)"
                
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getVideo(videoCode:String){
        let url = URL(string: "https://www.youtube.com/embed/\(videoCode)")
        webView.loadRequest(URLRequest(url:url!))
    }
    

   

}
